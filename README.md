# AV1_Adivinhacao_de_idade

Adivinhação da idade
 

Este trabalho remete a uma brincadeira de adivinhação. 
Trata-se de um programa que deve ser escrito em VisuAlg para poder 
adivinhar um determinado número que o participante pensou e também sua idade.

 
O programa deverá interagir com o participante fazendo três perguntas e 
disponibilizando um procedimento 
que deverá ser seguido passo a passo até o resultado final. 
Este será um número de três algarismos, no qual o algarismo da esquerda 
será o número que foi pensado e os outros dois irão 
revelar a idade do participante.